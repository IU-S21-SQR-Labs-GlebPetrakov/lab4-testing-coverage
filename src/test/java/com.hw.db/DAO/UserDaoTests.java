package com.hw.db.DAO;

import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class UserDaoTests {

    private JdbcTemplate jdbcMock;
    private UserDAO userDaoMock;

    @BeforeEach
    void init() {
        jdbcMock = mock(JdbcTemplate.class);
        userDaoMock = new UserDAO(jdbcMock);
    }

    @Test
    void userDaoChangeTest1() {
        User user = new User("jeff", null, null, null);
        UserDAO.Change(user);

        verifyNoInteractions(jdbcMock);
    }

    @Test
    void userDaoChangeTest2() {
        User user = new User("jeff", "jeff@jeff.jeff", null, null);
        UserDAO.Change(user);

        verify(jdbcMock)
                .update(eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
                        any(Object.class),
                        any(Object.class)
                );
    }

    @Test
    void userDaoChangeTest3() {
        User user = new User("jeff", null, "Jeff", null);
        UserDAO.Change(user);

        verify(jdbcMock)
                .update(eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
                        any(Object.class),
                        any(Object.class)
                );
    }

    @Test
    void userDaoChangeTest4() {
        User user = new User("jeff", null, null, "My name is Jeff");
        UserDAO.Change(user);

        verify(jdbcMock)
                .update(eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"),
                        any(Object.class),
                        any(Object.class)
                );
    }

    @Test
    void userDaoChangeTest5() {
        User user = new User("jeff", "jeff@jeff.jeff", "Jeff", null);
        UserDAO.Change(user);

        verify(jdbcMock)
                .update(eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"),
                        any(Object.class),
                        any(Object.class),
                        any(Object.class)
                );
    }

    @Test
    void userDaoChangeTest6() {
        User user = new User("jeff", "jeff@jeff.jeff", null, "My name is Jeff");
        UserDAO.Change(user);

        verify(jdbcMock)
                .update(eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"),
                        any(Object.class),
                        any(Object.class),
                        any(Object.class)
                );
    }

    @Test
    void userDaoChangeTest7() {
        User user = new User("jeff", null, "Jeff", "My name is Jeff");
        UserDAO.Change(user);

        verify(jdbcMock)
                .update(eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                        any(Object.class),
                        any(Object.class),
                        any(Object.class)
                );
    }

    @Test
    void userDaoChangeTest8() {
        User user = new User("jeff", "jeff@jeff.jeff", "Jeff", "My name is Jeff");
        UserDAO.Change(user);

        verify(jdbcMock)
                .update(eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                        any(Object.class),
                        any(Object.class),
                        any(Object.class),
                        any(Object.class)
                );
    }

}
