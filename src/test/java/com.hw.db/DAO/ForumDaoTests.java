package com.hw.db.DAO;

import com.hw.db.models.Forum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.*;

public class ForumDaoTests {

    private JdbcTemplate jdbcMock;
    private ForumDAO forumDaoMock;
    private UserDAO.UserMapper userMapperMock;
    private Forum forumMock;


    @BeforeEach
    void init() {
        jdbcMock = mock(JdbcTemplate.class);
        forumDaoMock = new ForumDAO(jdbcMock);
        userMapperMock = new UserDAO.UserMapper();
        forumMock = mock(Forum.class);
    }

    @Test
    void userListTest1() {
        ForumDAO.UserList(forumMock.getSlug(), null, null, null);

        verify(jdbcMock)
                .query(eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"),
                        any(Object[].class),
                        any(UserDAO.UserMapper.class)
                );
    }

    @Test
    void userListTest2() {
        ForumDAO.UserList(forumMock.getSlug(), 42, null, null);

        verify(jdbcMock)
                .query(eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;"),
                        any(Object[].class),
                        any(UserDAO.UserMapper.class)
                );
    }

    @Test
    void userListTest3() {
        ForumDAO.UserList(forumMock.getSlug(), 42, "1337", null);

        verify(jdbcMock)
                .query(eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"),
                        any(Object[].class),
                        any(UserDAO.UserMapper.class)
                );
    }

    @Test
    void userListTest4() {
        ForumDAO.UserList(forumMock.getSlug(), 42, "1337", true);

        verify(jdbcMock)
                .query(eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                        any(Object[].class),
                        any(UserDAO.UserMapper.class)
                );
    }

    @Test
    void userListTest5() {
        ForumDAO.UserList(forumMock.getSlug(), null, "1337", null);

        verify(jdbcMock)
                .query(eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
                        any(Object[].class),
                        any(UserDAO.UserMapper.class)
                );
    }

    @Test
    void userListTest6() {
        ForumDAO.UserList(forumMock.getSlug(), null, "1337", true);

        verify(jdbcMock)
                .query(eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"),
                        any(Object[].class),
                        any(UserDAO.UserMapper.class)
                );
    }

    @Test
    void userListTest7() {
        ForumDAO.UserList(forumMock.getSlug(), null, null, true);

        verify(jdbcMock)
                .query(eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc;"),
                        any(Object[].class),
                        any(UserDAO.UserMapper.class)
                );
    }

    @Test
    void userListTest8() {
        ForumDAO.UserList(forumMock.getSlug(), 42, null, true);

        verify(jdbcMock)
                .query(eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;"),
                        any(Object[].class),
                        any(UserDAO.UserMapper.class)
                );
    }

}
