package com.hw.db.DAO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class ThreadDaoTests {

    private JdbcTemplate jdbcMock;
    private ThreadDAO threadDaoMock;
    private ThreadDAO.ThreadMapper threadMapperMock;
    private PostDAO.PostMapper postMapperMock;

    @BeforeEach
    void init() {
        jdbcMock = mock(JdbcTemplate.class);
        threadDaoMock = new ThreadDAO(jdbcMock);
        threadMapperMock = new ThreadDAO.ThreadMapper();
        postMapperMock = new PostDAO.PostMapper();
    }

    @Test
    void threadDaoTreeSortTest1() {
        ThreadDAO.treeSort(1, 1, 1, false);

        verify(jdbcMock)
                .query(
                        eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"),
                        any(PostDAO.PostMapper.class),
                        any(Object.class),
                        any(Object.class),
                        any(Object.class)
                );
    }

    @Test
    void threadDaoTreeSortTest2() {
        ThreadDAO.treeSort(1, 1, 1, true);

        verify(jdbcMock)
                .query(
                        eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"),
                        any(PostDAO.PostMapper.class),
                        any(Object.class),
                        any(Object.class),
                        any(Object.class)
                );
    }
}
